package tk.developer.projects.pedometer.activities.configurator.presenters

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.preference.PreferenceManager
import tk.developer.projects.pedometer.activities.configurator.fragments.WeightHeightFragment
import tk.developer.projects.pedometer.activities.configurator.presenters.handlers.WeightHeightHandler
import tk.developer.projects.pedometer.providers.Enum.Body
import tk.developer.projects.pedometer.providers.Keys

class WeightHeightFragmentPresenter(private var weightHeightFragment: WeightHeightFragment)
    : WeightHeightHandler {
    private var sharedPreferences: SharedPreferences? = null
    private var editorSharedPreferences: SharedPreferences.Editor? = null
    override fun saveNewValueHeight(height: Int) {
        saveGenderToSharedPreferences(height, Body.HEIGHT)
    }

    override fun saveNewValueWeight(weight: Int) {
        saveGenderToSharedPreferences(weight, Body.WEIGHT)
    }

    private fun saveGenderToSharedPreferences(value: Int, weight: Body) {
        if (sharedPreferences == null)
            initSharedPreferences()
        if (weight == Body.WEIGHT)
            editorSharedPreferences!!.putInt(Keys.SHARED_PREFERENCES_WEIGHT, value).apply()
        else
            editorSharedPreferences!!.putInt(Keys.SHARED_PREFERENCES_HEIGHT, value).apply()
    }

    @SuppressLint("CommitPrefEdits")
    private fun initSharedPreferences() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(weightHeightFragment
                .context)
        editorSharedPreferences = sharedPreferences!!.edit()
    }
}