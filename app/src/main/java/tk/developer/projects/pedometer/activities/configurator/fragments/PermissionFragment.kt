package tk.developer.projects.pedometer.activities.configurator.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import tk.developer.projects.pedometer.R

class PermissionFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_permission, container, false)
    }

    fun newInstance(): PermissionFragment {
        val permissionFragment = PermissionFragment()
        val args = Bundle()
        permissionFragment.arguments = args
        return permissionFragment
    }
}
