package tk.developer.projects.pedometer.activities.safeViews

import tk.developer.projects.pedometer.activities.introconfigurator.safeViews.WeakActivity
import tk.developer.projects.pedometer.activities.configurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.views.GenderView
import tk.developer.projects.pedometer.providers.Enum.Gender

class GenderWeakActivity(activity: GenderFragment) : GenderView,
        WeakActivity<GenderFragment>(activity) {
    override fun setFocusAfterSelectGender(gender: Gender) {
        getStrongReference()?.setFocusAfterSelectGender(gender)
    }
}