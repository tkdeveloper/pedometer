package tk.developer.projects.pedometer.activities.configurator.presenters.handlers

interface WeightHeightHandler {
    fun saveNewValueHeight(height: Int)
    fun saveNewValueWeight(weight: Int)
}