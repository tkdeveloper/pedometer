package test.apps.android.freemium.com.testapp.activities.safeViews

import java.lang.ref.WeakReference

abstract class WeakActivity<T>(activity: T) {

    private val view: WeakReference<T> = WeakReference(activity)

    protected fun getStrongReference(): T?{
        return view.get()
    }
}