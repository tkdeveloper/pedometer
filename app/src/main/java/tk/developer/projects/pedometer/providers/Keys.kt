package tk.developer.projects.pedometer.providers

 object Keys {
     const val SHARED_PREFERENCES_FIRST_TIME: String = "sharedPreferencesFirstTime"
     const val SHARED_PREFERENCES_GENDER: String = "sharedPreferencesGender"
     const val SHARED_PREFERENCES_WEIGHT: String = "sharedPreferencesWeight"
     const val SHARED_PREFERENCES_HEIGHT: String = "sharedPreferencesHeight"
 }