package tk.developer.projects.pedometer.activities.mainconfigurator.helpers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.MainFragment
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.MapFragment
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.SavedFragment

class PagerAdapter(fm: FragmentManager, private val mainFragment: MainFragment,
                   private val savedFragment: SavedFragment,
                   private val mapFragment: MapFragment) : FragmentPagerAdapter(fm) {

    companion object {
        private const val NUM_ITEMS = 3
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> mainFragment
            1 -> savedFragment
            2 -> mapFragment
            else -> mainFragment
        }
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }
}