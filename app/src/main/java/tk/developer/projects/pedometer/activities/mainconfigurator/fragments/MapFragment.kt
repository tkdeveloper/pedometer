package tk.developer.projects.pedometer.activities.mainconfigurator.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tk.developer.projects.pedometer.R

class MapFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    fun newInstance(): MapFragment {
        val genderFragment = MapFragment()
        val args = Bundle()
        genderFragment.arguments = args
        return genderFragment
    }
}
