package tk.developer.projects.pedometer.activities.configurator.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shawnlin.numberpicker.NumberPicker

import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.configurator.presenters.WeightHeightFragmentPresenter

class WeightHeightFragment : Fragment() {
    private lateinit var weightHeightFragmentPresenter: WeightHeightFragmentPresenter
    private lateinit var pickerHeight: NumberPicker
    private lateinit var pickerWeight: NumberPicker
    private lateinit var viewFragment: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewFragment = inflater.inflate(R.layout.fragment_weight_height, container,
                false)
        pickerHeight = viewFragment.findViewById(R.id.height_picker)
        pickerWeight = viewFragment.findViewById(R.id.weight_picker)
        return viewFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListenersOnPickers()
        saveDefaultValueToPreferences()
    }

    private fun initPresenter() {
        weightHeightFragmentPresenter = WeightHeightFragmentPresenter(this)
    }

    private fun saveDefaultValueToPreferences() {
        weightHeightFragmentPresenter.saveNewValueHeight(pickerHeight.value)
        weightHeightFragmentPresenter.saveNewValueWeight(pickerWeight.value)
    }

    private fun initListenersOnPickers() {
        pickerHeight.setOnValueChangedListener { _, oldVal, newVal ->
            if (oldVal != newVal)
                weightHeightFragmentPresenter.saveNewValueHeight(newVal)
        }
        pickerWeight.setOnValueChangedListener { _, oldVal, newVal ->
            if (oldVal != newVal)
                weightHeightFragmentPresenter.saveNewValueWeight(newVal)
        }
    }

    fun newInstance(): WeightHeightFragment {
        val weightHeightFragment = WeightHeightFragment()
        val args = Bundle()
        weightHeightFragment.arguments = args
        return weightHeightFragment
    }
}
