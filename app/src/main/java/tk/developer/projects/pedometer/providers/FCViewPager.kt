package tk.developer.projects.pedometer.providers

import android.annotation.SuppressLint
import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


class FCViewPager : ViewPager {
    private var enableSwipe: Boolean = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        enableSwipe = true
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return enableSwipe && super.onInterceptTouchEvent(event)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return enableSwipe && super.onTouchEvent(event)
    }

    fun setEnableSwipe(enableSwipe: Boolean) {
        this.enableSwipe = enableSwipe
    }
}