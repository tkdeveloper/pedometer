package tk.developer.projects.pedometer.activities.introconfigurator.helpers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import tk.developer.projects.pedometer.activities.introconfigurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.introconfigurator.fragments.PermissionFragment
import tk.developer.projects.pedometer.activities.introconfigurator.fragments.WeightHeightFragment

class PagerAdapter(fm: FragmentManager, private val genderFragment: GenderFragment,
                   private val weightHeightFragment: WeightHeightFragment,
                   private val permissionFragment: PermissionFragment) : FragmentPagerAdapter(fm) {

    companion object {
        private const val NUM_ITEMS = 3
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> genderFragment
            1 -> weightHeightFragment
            2 -> permissionFragment
            else -> genderFragment
        }
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }
}