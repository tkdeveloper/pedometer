package tk.developer.projects.pedometer.activities.configurator

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import tk.developer.projects.pedometer.activities.configurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.configurator.fragments.PermissionFragment
import tk.developer.projects.pedometer.activities.configurator.fragments.WeightHeightFragment

class PagerAdapter(fm: FragmentManager, private val genderFragment: GenderFragment,
                   private val weightHeightFragment: WeightHeightFragment,
                   private val permissionFragment: PermissionFragment) : FragmentPagerAdapter(fm) {

    companion object {
        private const val NUM_ITEMS = 3
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            1 -> weightHeightFragment
            2 -> permissionFragment
            else -> genderFragment
        }
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }
}