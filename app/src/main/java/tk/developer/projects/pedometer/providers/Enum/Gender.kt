package tk.developer.projects.pedometer.providers.Enum

enum class Gender {
    MALE, FEMALE
}