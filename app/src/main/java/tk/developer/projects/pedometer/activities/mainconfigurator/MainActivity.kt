package tk.developer.projects.pedometer.activities.mainconfigurator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.MainFragment
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.MapFragment
import tk.developer.projects.pedometer.activities.mainconfigurator.fragments.SavedFragment
import tk.developer.projects.pedometer.activities.mainconfigurator.helpers.PagerAdapter
import tk.developer.projects.pedometer.activities.mainconfigurator.presenters.MainActivityPresenter
import tk.developer.projects.pedometer.providers.FCViewPager

class MainActivity : AppCompatActivity() {
    private lateinit var mainActivityPresenter: MainActivityPresenter
    private lateinit var mainFragment: MainFragment
    private lateinit var savedFragment: SavedFragment
    private lateinit var mapFragment: MapFragment
    private lateinit var viewPager: FCViewPager
    private lateinit var adapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()
        initFragments()
        initWidgets()
        initViewPager()
    }

    private fun initFragments() {
        mainFragment = MainFragment().newInstance()
        savedFragment = SavedFragment().newInstance()
        mapFragment = MapFragment().newInstance()
    }

    private fun initWidgets() {
    }

    private fun initPresenter() {
        mainActivityPresenter = MainActivityPresenter()
    }

    private fun initViewPager() {
        viewPager = findViewById(R.id.viewpager)
        adapter = PagerAdapter(supportFragmentManager, mainFragment,
                savedFragment, mapFragment)
        viewPager.adapter = adapter
        /*mainActivityPresenter.changeTitleAndDescription(0)*/
        viewPager.setEnableSwipe(false)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
               /* mainActivityPresenter.changeTitleAndDescription(position)
                if (position == 2)
                    mainActivityPresenter.askToChangeTextInPermissionFragment()*/
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }
}
