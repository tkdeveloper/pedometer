package tk.developer.projects.pedometer.activities.configurator

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.irozon.sneaker.Sneaker
import kotlinx.android.synthetic.main.activity_configurator.*
import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.configurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.configurator.fragments.PermissionFragment
import tk.developer.projects.pedometer.activities.configurator.fragments.WeightHeightFragment
import tk.developer.projects.pedometer.activities.configurator.presenters.handlers.OnGenderCallback
import tk.developer.projects.pedometer.activities.views.ConfiguratorView
import tk.developer.projects.pedometer.presenters.ConfiguratorPresenter
import tk.developer.projects.pedometer.providers.FCViewPager

class ConfiguratorActivity : AppCompatActivity(), ConfiguratorView, OnGenderCallback {
    private lateinit var configuratorPresenter: ConfiguratorPresenter
    private lateinit var viewPager: FCViewPager
    private lateinit var adapter: PagerAdapter
    private lateinit var genderFragment: GenderFragment
    private lateinit var weightHeightFragment: WeightHeightFragment
    private lateinit var permissionFragment: PermissionFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configurator)
        initPresenter()
        initFragments()
        initWidgets()
    }

    override fun setChangedTitleAndDescription(title: String, description: String) {
        fragmentTitle.text = title
        fragment_description.text = description
    }

    override fun notifyViewPagerToChangePage(page: Int) {
        viewPager.currentItem = page
    }

    override fun showSneakerToChooseGender() {
        Sneaker.with(this)
                .setTitle(resources.getString(R.string.error).toUpperCase())
                .setMessage(resources.getString(R.string.error_gender).capitalize())
                .sneakError()
    }

    override fun changeTextInPermissionFragment(string: String) {
        nextButton.text = string
    }

    override fun onGenderChanged() {
        nextButton.visibility = View.VISIBLE
    }

    private fun initPresenter() {
        configuratorPresenter = ConfiguratorPresenter(this)
    }

    private fun initFragments() {
        genderFragment = GenderFragment().newInstance()
        weightHeightFragment = WeightHeightFragment().newInstance()
        permissionFragment = PermissionFragment().newInstance()
    }

    private fun initWidgets() {
        supportActionBar?.hide()
        initViewPager()
        nextButton.setOnClickListener {
            if (genderFragment.isGenderSet)
                configuratorPresenter.goToNextFragment(viewPager.currentItem)
            else
                configuratorPresenter.askToChooseGender()
        }
    }

    private fun initViewPager() {
        viewPager = findViewById(R.id.viewpager)
        adapter = PagerAdapter(supportFragmentManager, genderFragment,
                weightHeightFragment, permissionFragment)
        viewPager.adapter = adapter
        configuratorPresenter.changeTitleAndDescription(0)
        viewPager.setEnableSwipe(false)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                configuratorPresenter.changeTitleAndDescription(position)
                if (position == 2)
                    configuratorPresenter.askToChangeTextInPermissionFragment()
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }
}
