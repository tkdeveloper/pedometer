package tk.developer.projects.pedometer.activities.introconfigurator.views

import tk.developer.projects.pedometer.providers.Enum.Gender

interface GenderView {
    fun setFocusAfterSelectGender(gender: Gender)
}