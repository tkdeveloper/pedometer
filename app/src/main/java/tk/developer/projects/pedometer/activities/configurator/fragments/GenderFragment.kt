package tk.developer.projects.pedometer.activities.configurator.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.configurator.presenters.GenderFragmentPresenter
import tk.developer.projects.pedometer.activities.views.GenderView
import tk.developer.projects.pedometer.providers.Enum.Gender

class GenderFragment : Fragment(), GenderView {
    private lateinit var genderFragmentPresenter: GenderFragmentPresenter
    private lateinit var viewFragment: View
    private lateinit var maleLayout: LinearLayout
    private lateinit var femaleLayout: LinearLayout
    private lateinit var maleImageView: ImageView
    private lateinit var femaleImageView: ImageView
    private lateinit var textMale: TextView
    private lateinit var textFemale: TextView
    var isGenderSet: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewFragment = inflater.inflate(R.layout.fragment_gender, container, false)
        maleLayout = viewFragment.findViewById(R.id.male_layout_linear)
        femaleLayout = viewFragment.findViewById(R.id.female_layout_linear)
        maleImageView = viewFragment.findViewById(R.id.maleImageView)
        femaleImageView = viewFragment.findViewById(R.id.femaleImageView)
        textMale = viewFragment.findViewById(R.id.maleTextView)
        textFemale = viewFragment.findViewById(R.id.femaleTextView)
        return viewFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    override fun setFocusAfterSelectGender(gender: Gender) {
        isGenderSet = true
        if (gender == Gender.MALE)
            setMaleLayout()
        else
            setFemaleLayout()
    }

    private fun setMaleLayout() {
        maleImageView.alpha = 1F
        textMale.alpha = 1F
        femaleImageView.alpha = 0.2F
        textFemale.alpha = 0.2F
    }

    private fun setFemaleLayout() {
        maleImageView.alpha = 0.2F
        textMale.alpha = 0.2F
        femaleImageView.alpha = 1F
        textFemale.alpha = 1F
    }

    private fun initPresenter() {
        genderFragmentPresenter = GenderFragmentPresenter(this)
    }

    private fun initElements() {
        maleLayout.setOnClickListener { genderFragmentPresenter.setGenderAs(Gender.MALE) }
        femaleLayout.setOnClickListener { genderFragmentPresenter.setGenderAs(Gender.FEMALE) }
    }

    fun newInstance(): GenderFragment {
        val genderFragment = GenderFragment()
        val args = Bundle()
        genderFragment.arguments = args
        return genderFragment
    }
}
