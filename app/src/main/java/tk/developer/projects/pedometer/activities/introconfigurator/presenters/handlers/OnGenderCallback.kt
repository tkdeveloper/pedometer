package tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers

interface OnGenderCallback {
    fun onGenderChanged()
}