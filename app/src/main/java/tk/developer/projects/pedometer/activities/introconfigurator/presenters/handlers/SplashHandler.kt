package tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers

interface SplashHandler {
    fun initMainSplashOrConfigurator()
}