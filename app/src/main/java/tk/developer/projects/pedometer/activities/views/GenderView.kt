package tk.developer.projects.pedometer.activities.views

import tk.developer.projects.pedometer.providers.Enum.Gender

interface GenderView {
    fun setFocusAfterSelectGender(gender: Gender)
}