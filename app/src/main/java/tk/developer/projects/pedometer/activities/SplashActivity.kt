package tk.developer.projects.pedometer.activities

import android.app.Activity
import android.os.Bundle
import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.presenters.SplashPresenter

class SplashActivity : Activity() {
    private lateinit var splashPresenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initSplashPresenter()
    }

    private fun initSplashPresenter() {
        splashPresenter = SplashPresenter(this)
        splashPresenter.initMainSplashOrConfigurator()
    }
}
