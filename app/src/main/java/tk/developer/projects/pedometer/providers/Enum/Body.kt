package tk.developer.projects.pedometer.providers.Enum

enum class Body {
    WEIGHT, HEIGHT
}