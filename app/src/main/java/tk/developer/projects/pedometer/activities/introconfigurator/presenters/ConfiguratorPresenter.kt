package tk.developer.projects.pedometer.activities.introconfigurator.presenters

import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.introconfigurator.ConfiguratorActivity
import tk.developer.projects.pedometer.activities.introconfigurator.safeViews.ConfiguratorWeakActivity
import tk.developer.projects.pedometer.activities.introconfigurator.views.ConfiguratorView
import tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers.ConfiguratorHandler

class ConfiguratorPresenter(private var configuratorActivity: ConfiguratorActivity)
    : ConfiguratorHandler {
    private var configuratorView: ConfiguratorView = ConfiguratorWeakActivity(configuratorActivity)

    override fun changeTitleAndDescription(fragmentId: Int) {
        configuratorView.setChangedTitleAndDescription(chooseTitle(fragmentId),
                chooseDescription(fragmentId))
    }

    override fun goToNextFragment(currentItem: Int) {
        when (currentItem) {
            2 -> configuratorView.finishConfigurator()
            else -> configuratorView.notifyViewPagerToChangePage(currentItem + 1)
        }
    }

    override fun askToChooseGender() {
        configuratorView.showSneakerToChooseGender()
    }

    override fun askToChangeTextInPermissionFragment() {
        configuratorView.changeTextInPermissionFragment(configuratorActivity
                .resources.getString(R.string.start))
    }

    private fun chooseDescription(id: Int): String {
        return when (id) {
            0 -> configuratorActivity.resources.getString(R.string.gender_description).capitalize()
            1 -> configuratorActivity.resources.getString(R.string.weight_height_description).capitalize()
            2 -> configuratorActivity.resources.getString(R.string.permission_description).capitalize()
            else -> ""
        }
    }

    private fun chooseTitle(id: Int): String {
        return when (id) {
            0 -> configuratorActivity.resources.getString(R.string.gender_title).capitalize()
            1 -> configuratorActivity.resources.getString(R.string.weight_height_title).capitalize()
            2 -> configuratorActivity.resources.getString(R.string.permission_title).capitalize()
            else -> ""
        }
    }
}