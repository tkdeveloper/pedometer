package tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers

import tk.developer.projects.pedometer.providers.Enum.Gender

interface GenderHandler {
    fun setGenderAs(gender: Gender)
}