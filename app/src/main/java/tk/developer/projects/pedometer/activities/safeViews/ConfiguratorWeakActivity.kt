package tk.developer.projects.pedometer.activities.safeViews

import tk.developer.projects.pedometer.activities.introconfigurator.safeViews.WeakActivity
import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.configurator.ConfiguratorActivity
import tk.developer.projects.pedometer.activities.views.ConfiguratorView

class ConfiguratorWeakActivity(var activity: ConfiguratorActivity) : ConfiguratorView,
        WeakActivity<ConfiguratorView>(activity) {
    override fun changeTextInPermissionFragment(string: String) {
        return getStrongReference()?.changeTextInPermissionFragment(
                activity.resources.getString(R.string.start))!!
    }

    override fun notifyViewPagerToChangePage(page: Int) {
        return getStrongReference()?.notifyViewPagerToChangePage(page)!!
    }

    override fun setChangedTitleAndDescription(title: String, description: String) {
        return getStrongReference()?.setChangedTitleAndDescription(title, description)!!
    }

    override fun showSneakerToChooseGender() {
        return getStrongReference()?.showSneakerToChooseGender()!!
    }
}