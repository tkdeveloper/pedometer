package tk.developer.projects.pedometer.activities.introconfigurator.safeViews

import tk.developer.projects.pedometer.activities.introconfigurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.introconfigurator.views.GenderView
import tk.developer.projects.pedometer.providers.Enum.Gender

class GenderWeakActivity(activity: GenderFragment) : GenderView,
        WeakActivity<GenderFragment>(activity) {
    override fun setFocusAfterSelectGender(gender: Gender) {
        getStrongReference()?.setFocusAfterSelectGender(gender)
    }
}