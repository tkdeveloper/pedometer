package tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers

interface ConfiguratorHandler {
    fun changeTitleAndDescription(fragmentId: Int)
    fun goToNextFragment(currentItem: Int)
    fun askToChooseGender()
    fun askToChangeTextInPermissionFragment()
}