package tk.developer.projects.pedometer.activities.configurator.presenters

import android.content.SharedPreferences
import android.preference.PreferenceManager
import tk.developer.projects.pedometer.activities.configurator.fragments.GenderFragment
import tk.developer.projects.pedometer.activities.configurator.presenters.handlers.GenderHandler
import tk.developer.projects.pedometer.activities.configurator.presenters.handlers.OnGenderCallback
import tk.developer.projects.pedometer.activities.safeViews.GenderWeakActivity
import tk.developer.projects.pedometer.activities.views.GenderView
import tk.developer.projects.pedometer.providers.Enum.Gender
import tk.developer.projects.pedometer.providers.Keys

class GenderFragmentPresenter(private var genderFragment: GenderFragment) : GenderHandler {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editorSharedPreferences: SharedPreferences.Editor
    private var genderView: GenderView = GenderWeakActivity(genderFragment)
    private var onGenderCallback: OnGenderCallback = genderFragment.context as OnGenderCallback

    override fun setGenderAs(gender: Gender) {
        saveGenderToSharedPreferences(gender)
        genderView.setFocusAfterSelectGender(gender)
        onGenderCallback.onGenderChanged()
    }

    private fun saveGenderToSharedPreferences(gender: Gender) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(genderFragment.context)
        editorSharedPreferences = sharedPreferences.edit()
        editorSharedPreferences.putString(Keys.SHARED_PREFERENCES_GENDER, gender.toString()).apply()
    }
}