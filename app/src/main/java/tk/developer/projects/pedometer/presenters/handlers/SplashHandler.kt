package tk.developer.projects.pedometer.presenters.handlers

interface SplashHandler {
    fun initMainSplashOrConfigurator()
}