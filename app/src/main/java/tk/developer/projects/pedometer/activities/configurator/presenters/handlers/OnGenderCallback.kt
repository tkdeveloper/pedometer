package tk.developer.projects.pedometer.activities.configurator.presenters.handlers

interface OnGenderCallback {
    fun onGenderChanged()
}