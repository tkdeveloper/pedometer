package tk.developer.projects.pedometer.presenters

import android.content.SharedPreferences
import android.preference.PreferenceManager
import tk.developer.projects.pedometer.activities.SplashActivity
import tk.developer.projects.pedometer.presenters.handlers.SplashHandler
import tk.developer.projects.pedometer.providers.Keys.SHARED_PREFERENCES_FIRST_TIME
import android.content.Intent
import tk.developer.projects.pedometer.activities.MainActivity
import tk.developer.projects.pedometer.activities.configurator.ConfiguratorActivity

class SplashPresenter(private var splashActivity: SplashActivity) : SplashHandler {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editorSharedPreferences: SharedPreferences.Editor
    private lateinit var intent: Intent
    private var isFirstTime = false

    override fun initMainSplashOrConfigurator() {
        initSharePreferences()
    }

    private fun initSharePreferences() {
        sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(splashActivity.applicationContext)

        isFirstTime = sharedPreferences.getBoolean(SHARED_PREFERENCES_FIRST_TIME, true)

        intent = if (isFirstTime) {
            saveFirstTimeToSharedPreferences()
            Intent(splashActivity, ConfiguratorActivity::class.java)
        } else {
            Intent(splashActivity, MainActivity::class.java)
        }

        intent = Intent(splashActivity, ConfiguratorActivity::class.java)
        splashActivity.startActivity(intent)
    }

    private fun saveFirstTimeToSharedPreferences() {
        editorSharedPreferences = sharedPreferences.edit()
        editorSharedPreferences.putBoolean(SHARED_PREFERENCES_FIRST_TIME, false).apply()
    }
}