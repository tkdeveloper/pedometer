package tk.developer.projects.pedometer.activities.introconfigurator.presenters.handlers

interface WeightHeightHandler {
    fun saveNewValueHeight(height: Int)
    fun saveNewValueWeight(weight: Int)
}