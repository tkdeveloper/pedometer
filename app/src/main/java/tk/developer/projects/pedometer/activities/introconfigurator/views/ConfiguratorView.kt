package tk.developer.projects.pedometer.activities.introconfigurator.views

interface ConfiguratorView {
    fun setChangedTitleAndDescription(title: String, description: String)
    fun notifyViewPagerToChangePage(page: Int)
    fun showSneakerToChooseGender()
    fun changeTextInPermissionFragment(string: String)
    fun finishConfigurator()
}