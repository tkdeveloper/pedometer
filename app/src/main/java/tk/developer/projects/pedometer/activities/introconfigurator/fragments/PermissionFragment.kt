package tk.developer.projects.pedometer.activities.introconfigurator.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import tk.developer.projects.pedometer.R
import tk.developer.projects.pedometer.activities.introconfigurator.presenters.PermissionFragmentPresenter

class PermissionFragment : Fragment() {
    private lateinit var permissionFragmentPresenter: PermissionFragmentPresenter
    private lateinit var viewFragment: View
    private lateinit var startBt: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionFragmentPresenter = PermissionFragmentPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewFragment = inflater.inflate(R.layout.fragment_permission, container, false)
        startBt = viewFragment.findViewById(R.id.start)
        return viewFragment
    }

    fun newInstance(): PermissionFragment {
        val permissionFragment = PermissionFragment()
        val args = Bundle()
        permissionFragment.arguments = args
        return permissionFragment
    }
}
